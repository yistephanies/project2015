###A brief Summary of the existing spatial extensions of the considered DBMS
| DBMS/FUNCTIONS  |  PostGIS(extending PostGRES) | MonetDB (geos module) | SpatiaLite(extending SQLite)  | Oracle Spatial and Graph  |IBM DB2| Microsoft SQL Server
|---|---|---|---|---|
|type|open source|open source|open source|commercial| commercial |commercial |
| built-in-support for spatial point object data type | yes | yes  |yes   |yes   |yes| yes |
| built-in-support for spatial data model (Geometry) | 2D & 3D   | 2D only  | 2D only   |  2D & 3D   | 2D only| 2D only |     
| expression for spatial objects) | 2D & 3D   | WKT only | 2D only   |  2D & 3D   | 2D only| 2D only |   
|Customizing spatial index|GiST and B-tree|R|[R*tree](http://sqlite.org/rtree.html)|R|r|R
|default spatial indexing method |  A generalized Rtree based on [GiST](http://www.postgresql.org/docs/current/interactive/gist.html) | no  special accelerator to spatial objects, need to use MBR  | [VirtualSpatialIndex](based on VirtualSpatialIndex.)   | Rtree   |grid index using MBR(2D)| grid hierachy and Btree
| support for spatial functions (e.g.range queries,join,k-n-n) | Simple Features Access standard of (OGC)  |Simple Features Access standard of (OGC)  |support for the Simple Features Access standard of (OGC)   | Simple Features Access standard of (OGC) | Simple Features Access standard of (OGC) |Simple Features Access standard of (OGC)|

#Simple Features Access standard of OGC

geometry1.STContains(geometry2) = 1     
geometry1.STDistance(geometry2) < number        
geometry1.STDistance(geometry2) <= number       
geometry1.STEquals(geometry2) = 1       
geometry1.STIntersects(geometry2) = 1       
geometry1.STOverlaps(geometry2) = 1     
geometry1.STTouches(geometry2) = 1      
geometry1.STWithin(geometry2) = 1     